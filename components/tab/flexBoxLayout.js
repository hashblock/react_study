import React, { Component } from 'react';

import { StyleSheet, Text, View, ScrollView } from 'react-native';
import Tabs from './tabs';


export default class flexBoxLayout extends Component {


    render() {
        return (
            <ScrollView>
            <View style={styles.container}>
                <Tabs>
                    {/* First tab */}
                    <View title="Flex Direction" style={styles.content}>
                        <Text style={styles.header}>
                            Flex Direction - row
                        </Text>
                        <View style={{
                            flex: 1, flexDirection: 'row'
                        }}>
                            <View style={{width: 50, height: 50, backgroundColor: 'powderblue'}} />
                            <View style={{width: 50, height: 50, backgroundColor: 'skyblue'}} />
                            <View style={{width: 50, height: 50, backgroundColor: 'steelblue'}} />
                        </View>
                        <Text style={styles.header}>
                            Flex Direction - column
                        </Text>
                        <View style={{
                            flex: 1, flexDirection: 'column'
                        }}>
                            <View style={{width: 50, height: 50, backgroundColor: 'powderblue'}} />
                            <View style={{width: 50, height: 50, backgroundColor: 'skyblue'}} />
                            <View style={{width: 50, height: 50, backgroundColor: 'steelblue'}} />
                        </View>
                    </View>

                    {/* Second tab */}
                    <View title="Justify Content" style={styles.content}>
                        <Text style={styles.header}>
                            Justify Content - flex-start
                        </Text>
                        <View style={{height: 250, }}>
                            <View style={{
                                flex: 1,
                                flexDirection: 'column',
                                justifyContent: 'flex-start',
                            }}>
                                <View style={{width: 50, height: 50, backgroundColor: 'powderblue'}} />
                                <View style={{width: 50, height: 50, backgroundColor: 'skyblue'}} />
                                <View style={{width: 50, height: 50, backgroundColor: 'steelblue'}} />
                            </View>
                        </View>


                        <Text style={styles.header}>
                            Justify Content - center
                        </Text>
                        <View style={{height: 250, }}>
                            <View style={{
                                flex: 1,
                                flexDirection: 'column',
                                justifyContent: 'center',
                            }}>
                                <View style={{width: 50, height: 50, backgroundColor: 'powderblue'}} />
                                <View style={{width: 50, height: 50, backgroundColor: 'skyblue'}} />
                                <View style={{width: 50, height: 50, backgroundColor: 'steelblue'}} />
                            </View>
                        </View>

                        <Text style={styles.header}>
                            Justify Content - flex-end
                        </Text>
                        <View style={{height: 250, }}>
                            <View style={{
                                flex: 1,
                                flexDirection: 'column',
                                justifyContent: 'flex-end',
                            }}>
                                <View style={{width: 50, height: 50, backgroundColor: 'powderblue'}} />
                                <View style={{width: 50, height: 50, backgroundColor: 'skyblue'}} />
                                <View style={{width: 50, height: 50, backgroundColor: 'steelblue'}} />
                            </View>
                        </View>

                        <Text style={styles.header}>
                            Justify Content - space-around
                        </Text>
                        <View style={{height: 250, }}>
                            <View style={{
                                flex: 1,
                                flexDirection: 'column',
                                justifyContent: 'space-around',
                            }}>
                                <View style={{width: 50, height: 50, backgroundColor: 'powderblue'}} />
                                <View style={{width: 50, height: 50, backgroundColor: 'skyblue'}} />
                                <View style={{width: 50, height: 50, backgroundColor: 'steelblue'}} />
                            </View>
                        </View>

                        <Text style={styles.header}>
                            Justify Content - space-between
                        </Text>
                        <View style={{height: 250, }}>
                            <View style={{
                                flex: 1,
                                flexDirection: 'column',
                                justifyContent: 'space-between',
                            }}>
                                <View style={{width: 50, height: 50, backgroundColor: 'powderblue'}} />
                                <View style={{width: 50, height: 50, backgroundColor: 'skyblue'}} />
                                <View style={{width: 50, height: 50, backgroundColor: 'steelblue'}} />
                            </View>
                        </View>


                    </View>


                    {/* Third tab */}
                    <View title="Align Items" style={styles.content}>
                        <Text style={styles.header}>
                            Align Items - flex-start
                        </Text>
                        <View style={{height: 200 }}>
                            <View style={{
                                flex: 1,
                                flexDirection: 'column',
                                justifyContent: 'center',
                                alignItems: 'flex-start',
                            }}>
                                <View style={{width: 50, height: 50, backgroundColor: 'powderblue'}} />
                                <View style={{width: 50, height: 50, backgroundColor: 'skyblue'}} />
                                <View style={{width: 50, height: 50, backgroundColor: 'steelblue'}} />
                            </View>
                        </View>

                        <Text style={styles.header}>
                            Align Items - center
                        </Text>
                        <View style={{height: 200 }}>
                            <View style={{
                                flex: 1,
                                flexDirection: 'column',
                                justifyContent: 'center',
                                alignItems: 'center',
                            }}>
                                <View style={{width: 50, height: 50, backgroundColor: 'powderblue'}} />
                                <View style={{width: 50, height: 50, backgroundColor: 'skyblue'}} />
                                <View style={{width: 50, height: 50, backgroundColor: 'steelblue'}} />
                            </View>
                        </View>

                        <Text style={styles.header}>
                            Align Items - flex-end
                        </Text>
                        <View style={{height: 200 }}>
                            <View style={{
                                flex: 1,
                                flexDirection: 'column',
                                justifyContent: 'center',
                                alignItems: 'flex-end',
                            }}>
                                <View style={{width: 50, height: 50, backgroundColor: 'powderblue'}} />
                                <View style={{width: 50, height: 50, backgroundColor: 'skyblue'}} />
                                <View style={{width: 50, height: 50, backgroundColor: 'steelblue'}} />
                            </View>
                        </View>

                        <Text style={styles.header}>
                            Align Items - stretch
                        </Text>
                        <View style={{height: 200 }}>
                            <View style={{
                                flex: 1,
                                flexDirection: 'column',
                                justifyContent: 'center',
                                alignItems: 'stretch',
                            }}>
                                <View style={{width: 50, height: 50, backgroundColor: 'powderblue'}} />
                                <View style={{width: 50, height: 50, backgroundColor: 'skyblue'}} />
                                <View style={{width: 50, height: 50, backgroundColor: 'steelblue'}} />
                            </View>
                        </View>
                    </View>

                </Tabs>
            </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#E91E63',
    },

    content: {
        backgroundColor: '#C2185B',
    },

    header: {
        color: '#FFFFFF',
        fontFamily: 'Avenir',
        fontSize: 26,
    },
});