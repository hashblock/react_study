import { AppRegistry } from 'react-native';
import app from './app';

AppRegistry.registerComponent('election_survey_app', () => app);
